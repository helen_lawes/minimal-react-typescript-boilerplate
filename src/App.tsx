import * as React from 'react';

import StyledApp from './App.styles';

const App: React.FunctionComponent = () => <StyledApp>App works!</StyledApp>;

export default App;
