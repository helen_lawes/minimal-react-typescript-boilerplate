import { Component } from 'react';
import * as Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

declare module 'enzyme' {
	interface ReactWrapper<P = {}, S = {}, C = Component> {
		findByText(string: string): any;
	}
}

Enzyme.ReactWrapper.prototype.findByText = function(string: string) {
	return this.findWhere((n: any) => {
		const domNode = n.getDOMNode();
		return domNode
			? domNode.TEXT_NODE &&
					domNode.childNodes.length &&
					domNode.childNodes[0].nodeName === '#text' &&
					domNode.textContent === string
			: false;
	});
};
