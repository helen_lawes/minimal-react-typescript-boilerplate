module.exports = {
	testRegex: '/src/.*?(test)\\.tsx$',
	modulePathIgnorePatterns: ['node_modules', 'dist'],
	setupFiles: ['<rootDir>/jest-setup/index.ts'],
	snapshotSerializers: ['enzyme-to-json/serializer'],
};
